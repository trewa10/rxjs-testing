import {combineLatest, combineLatestWith, from } from "rxjs"; 


describe('combineLatest', () => {

  test('should return array of latest values from the source and combined stream', () => {
    // s1$ --1--2--3--|-->         s1$.pipe(combineLatestWith(s2$)) 
    // s2$ ----------4--5--6--|-->                                   --[3, 4]--[3, 5]--[3, 6]--|-->

    // s1$ -----1--2--3--|-->  
    // s2$ --4--5--------6--|-->                                     --[1, 5]--[2, 5]--[3, 5]--[3, 6]--|-->
  
    
    const s1$ = from([1, 2, 3]);
    const s2$ = from([4, 5, 6]);

    const results = [[3, 4], [3, 5], [3, 6]]; 
    const values1 = [];
    const values2 = [];

    combineLatest([s1$, s2$]).subscribe(val => {   
      values1.push(val)
    })


    s1$.pipe(combineLatestWith(s2$))            
    .subscribe(val => values2.push(val))

    expect(values1).toEqual(results);
    expect(values2).toEqual(results);
  });
  
});