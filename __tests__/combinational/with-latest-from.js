import {withLatestFrom, from } from "rxjs"; 


describe('withLatestFrom', () => {

  test('should return array of latest value from combined stream and current value from the source', () => {
    // source$ --1------------2--3-----4--|-->         
    // s2$ --------4-----5-----------6----|-->             --[2, 5]--[3, 5]--[4, 6]--|-->

    
    
    const source$ = from([1, 2, 3]);
    const s2$ = from([4, 5, 6]);

    const results = [[1, 6], [2, 6], [3, 6]]; 
    const values = [];

    source$.pipe(withLatestFrom(s2$))            
    .subscribe(val => {
      // console.log(val);
      values.push(val)
    })

    expect(values).toEqual(results);
  });
  
});