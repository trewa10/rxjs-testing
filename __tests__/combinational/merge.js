import {merge, mergeWith, from, delay } from "rxjs"; 


describe('merge', () => {

  test('should return values from the source and merged streams', done => {
    // s1$ --1--2--3--|-->  merge(s1$, s2$, s3$)
    // s2$ --4--5--6--|-->  s1$.pipe(mergeWith(s2$, s3$)) --1--2--3--4--5--6--7--8--9--|-->
    // s3$ --7--8--9--|-->
    
    const s1$ = from([1, 2, 3]).pipe(delay(500));
    // const s1$ = from([1, 2, 3]);
    const s2$ = from([4, 5, 6]).pipe(delay(600));
    const s3$ = from([7, 8, 9]);

    let i = 0;
    let j = 0;
    // const results = [1, 2, 3, 4, 5, 6, 7, 8, 9]; 
    const results = [7, 8, 9, 1, 2, 3, 4, 5, 6];

    merge(s1$, s2$, s3$).subscribe({         // deprecated
      next: val => {   
        expect(val).toEqual(results[i]);
        i++;
      },
      complete: () => done()
    })


    s1$.pipe(mergeWith(s2$, s3$))            // the same
    .subscribe({
      next: val => {
        expect(val).toEqual(results[j]);
        j++;
      }
    })

  });
  
});