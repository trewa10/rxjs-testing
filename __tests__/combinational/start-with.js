import {startWith, from } from "rxjs"; 


describe('startWith', () => {

  test('should return own arguments and then all values from stream', () => {
    // s1$ --1--2--3--|-->         
    // startWith(5)               --5--1--2--3--|-->
  
    
    const s1$ = from([1, 2, 3]);

    const results = [5, 8, [], 1, 2, 3];
    const vals = [];

    s1$.pipe(startWith(5, 8, []))
    .subscribe({
      next: val => {   
        vals.push(val)
      }
    })

    expect(vals).toEqual(results);
  
  });
  
});