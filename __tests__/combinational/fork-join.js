import {forkJoin, from } from "rxjs"; 


describe('forkJoin', () => {

  test('should return array of latest values from the streams after complete all of them', () => {
    // s1$ --1--2--3--|-->         
    // s2$ ----------4--5--6--|-->          --[3, 6, 9]--|-->
    // s3$ -----7--8--9--|--> 
  
    
    const s1$ = from([1, 2, 3]);
    const s2$ = from([4, 5, 6]);
    const s3$ = from([7, 8, 9]);

    let completed = false;
    const results = [3, 6, 9]; 

    // forkJoin(s1$, s2$)  // deprecated

    forkJoin([s1$, s2$, s3$])
    .subscribe({
      next: val => {   
        expect(val).toEqual(results);
      },
      complete: () => completed = true
    })

    expect(completed).toEqual(true);
  
  });
  
});