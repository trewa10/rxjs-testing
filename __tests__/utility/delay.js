import { from, delay, map, Subject, mergeWith } from "rxjs"; 


describe('delay', () => {

  test('should return values by the specified timeout', done => {
    // s1$ --1--2--3--4--5--|-->  delay(2000) 
    //                                        --2sec--1--2--3--4--5--|-->
    
    const values = [1, 2, 3, 4, 5];
    const s1$ = from(values).pipe(delay(1000));
    let i = 0;

    s1$.subscribe({
      next: val => {
        expect(val).toEqual(values[i]);
        i++;
      },
      complete: done
    })

  });

  test('should return values by the specified date', done => {
    // s1$ --1--2--3--4--5--|-->  delay(date) 
    //                                        --(now === date)--1--2--3--4--5--|-->
    
    const values = [1, 2, 3, 4, 5];
    const delayedDate = new Date(Date.now() + 700);
    const s1$ = from(values).pipe(delay(delayedDate));
    let i = 0;

    s1$.subscribe({
      next: val => {
        expect(val).toEqual(values[i]);
        i++;
      },
      complete: done
    })

  });

  test('should not do nothing with error ', done => {
    const values = [1, 2, 3, 4, 5];

    const s1$ = from(values).pipe(delay(500));
    let i = 0;
    let error = 'Some bug';
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 600)

    s1$.pipe(mergeWith(s2$))            
    .subscribe({
      next: val => {
        expect(val).toEqual(values[i]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      },
      complete: done
    })

  });
});