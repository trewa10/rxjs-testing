import { from, tap, Subject, mergeWith } from 'rxjs';

describe('tap', () => {
  test('should get values from observable and return the same source ', () => {
    // s1$ --1--2--3--4--5--|-->  tap(callback)
    //                                           --1--2--3--4--5--|-->

    const values = [1, 2, 3, 4, 5];
    const s1$ = from(values);
    let i = 0;

    s1$
      .pipe(
        tap((val) => {
          expect(val).toEqual(values[i]);
        })
      )
      .subscribe({
        next: (val) => {
          expect(val).toEqual(values[i]);
          i++;
        },
      });
  });

  test('should catch error ', () => {
    let error = 'Some bug';

    const s2$ = new Subject();
    s2$.error(new Error(error));

    s2$
      .pipe(
        tap({
          error: (err) => expect(err.message).toEqual(error),
        })
      )
      .subscribe({
        error: (err) => {
          expect(err.message).toEqual(error);
        },
      });
  });

  test('should catch complete ', () => {
    let completed = false;
    const values = [1, 2, 3, 4, 5];
    const s1$ = from(values);
    let i = 0;

    s1$
      .pipe(
        tap({
          complete: () => (completed = true),
        })
      )
      .subscribe({
        next: (val) => {
          // expect(val).toEqual(values[i]);
          i++;
        },
      });

    expect(completed).toEqual(true);
  });
});
