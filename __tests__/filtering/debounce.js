import {
  from,
  debounce,
  interval,
  timer,
  Subject,
  mergeWith,
  take,
} from 'rxjs';

describe('debounce', () => {
  test('should return value with delay for another stream', (done) => {
    // s1$ --1--1--1--2--2--2--1--1--3--3--|-->
    // s2$ --------1--------2-----3------->
    //                                           debounce()
    //                                                       

    const results = [0, 1, 2, 3];
    const s1$ = interval(500).pipe(take(4));
    let i = 0;

    s1$.pipe(debounce((val) => timer(val * 100))).subscribe({
      next: (val) => {
        console.log(val);
        expect(val).toEqual(results[i]);
        i++;
      },
      complete: done,
    });
  });

  
  test('should not do nothing with error ', done => {
    const values = [1, 2, 3, 4, 5];
    let i = 0;
    const error = 'Some bug';
    const s1$ = from(values);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 1000)

    s1$.pipe(
      mergeWith(s2$),
      debounce((val) => timer(val * 100))
      )
    .subscribe({
      next: val => {
        expect(val).toEqual(values[values.length - 1]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      }
    })

  });
});
