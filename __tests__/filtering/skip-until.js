import { from, skipUntil, Subject, mergeWith, interval, take } from 'rxjs';

describe('skipUntil', () => {
  test('should return only values after another stream returns value', done => {
    // s1$ --1--2--3--4--5--|-->
    // s2$ ----------1------|-->
    //                           skipUntil(s2$)
    //                                           --4--5--|-->

    const s1$ = interval(100);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.next(1);
    }, 300)

    const results = [2, 3, 4];
    let i = 0;

    s1$.pipe(
      skipUntil(s2$),
      take(3)
      )
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      complete: done
    })

  });

  test('should throw error', done => {
    const s1$ = interval(100);
    const s2$ = new Subject();
    const error = 'Some bug';
    let isNext = false;

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 300);

    s1$.pipe(
        skipUntil(s2$), 
        take(3)
      )
      .subscribe({
      next: (val) => {
        isNext = true;
      },
      error: (err) => {
        expect(err.message).toEqual(error);
        done();
      }
    });

    expect(isNext).not.toEqual(true);
  });

});
