import { takeUntil, Subject, interval } from 'rxjs';

describe('takeUntil', () => {
  test('should return only values before another stream returns a value', done => {
    // s1$ --1--2--3--4--5--|-->
    // s2$ ----------1------|-->
    //                           takeUntil(s2$)
    //                                           --1--2--3--|-->

    const s1$ = interval(100);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.next(1);
    }, 350)

    const results = [0, 1, 2];
    let i = 0;

    s1$.pipe(
      takeUntil(s2$)
      )
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      complete: done
    })

  });

  test('should throw error', done => {
    const s1$ = interval(100);
    const s2$ = new Subject();
    const error = 'Some bug';

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 350);

    const results = [0, 1, 2];
    let i = 0;

    s1$.pipe(
      takeUntil(s2$)
      )
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      error: (err) => {
        expect(err.message).toEqual(error);
        done();
      }
    });

  });

});
