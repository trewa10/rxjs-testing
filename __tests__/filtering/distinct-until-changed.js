import { from, distinctUntilChanged, Subject, mergeWith } from "rxjs"; 


describe('distinctUntilChanged', () => {

  test('should return only values that are different from previous', () => {
    // s1$ --1--1--1--2--2--2--1--1--3--3--|-->  
    //                                           distinctUntilChanged() 
    //                                                                   --1--2--1--3--|-->

    // -- val === prevVal --> X
    // -- val !== prevVal --> val
    
    const values = [1, 1, 1, 2, 2, 2, 1, 1, 3, 3];
    const results = [1, 2, 1, 3];
    const s1$ = from(values);
    let i = 0;

    s1$.pipe(distinctUntilChanged())
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should work with custom Сomparator', () => {
    
    const values = [{id: 1}, {id: 1}, {id: 2}, {id: 2}, {id: 3}];
    const results = [{id: 1}, {id: 2}, {id: 3}];
    const s1$ = from(values);
    let i = 0;

    s1$.pipe(distinctUntilChanged((prev, curr) => {
      return prev.id === curr.id
    }))
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should work with keySelector', () => {
    
    const values = [{id: 1, user: 'Sem'}, {id: 1, user: 'Sem'}, {id: 1, user: 'John'}, {id: 1, user: 'John'}, {id: 1, user: 'Sem'}, {id: 1, user: 'Din'}];
    const results = [{id: 1, user: 'Sem'}, {id: 1, user: 'John'}, {id: 1, user: 'Sem'}, {id: 1, user: 'Din'}];
    const s1$ = from(values);
    let i = 0;

    s1$.pipe(distinctUntilChanged(undefined, obj => obj.user))
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should not do nothing with error ', done => {
    const values = [1, 1, 1, 2, 2, 2, 1, 1, 3, 3];
    const results = [1, 2, 1, 3];
    let i = 0;
    const error = 'Some bug';
    const s1$ = from(values);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 500)

    s1$.pipe(
      mergeWith(s2$),
      distinctUntilChanged()
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      }
    })

  });
});