import { from, skip, Subject, mergeWith, filter } from "rxjs"; 


describe('skip', () => {

  test('should return only values after count', () => {
    // s1$ --1--2--3--4--5--|-->  skip(2) 
    //                                     --3--4--5--|-->
    
    const values = [1, 2, 3, 4, 5];
    const s1$ = from(values);
    const results = [3, 4, 5];
    let i = 0;

    s1$.pipe(
      skip(2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should works like filter', () => {

    const values = [1, 2, 3, 4, 5];
    const s1$ = from(values);
  
    const results = [3, 4, 5];
    let i = 0;

    s1$.pipe(
      filter((v, index) => index >= 2)              // same as skip(2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should not do nothing with error ', done => {
    const values = [1, 2, 3, 4, 5];
    const results = [3, 4, 5];
    let i = 0;
    const error = 'Some bug';
    const s1$ = from(values);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 500)

    s1$.pipe(
      mergeWith(s2$),
      skip(2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      }
    })

  });
});