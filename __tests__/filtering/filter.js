import { from, filter, Subject, mergeWith } from "rxjs"; 


describe('filter', () => {

  test('should return only values that satisfy the specified callback and works the same as JS filter', () => {
    // s1$ --1--2--3--4--5--|-->  filter(v => v % 2 !== 0) 
    //                                                     --1--3--5--|-->
    
    const values = [1, 2, 3, 4, 5];

    const s1$ = from(values);
  
    const results = values.filter(v => v % 2 !== 0);
    let i = 0;

    s1$.pipe(
      filter(v => v % 2 !== 0)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should work with index', () => {
    // s1$ --1--2--3--4--5--|-->  
    //                            filter(v => v % 2 !== 0 && i !== 2) 
    //                                                                --1--5--|-->
    
    const values = [1, 2, 3, 4, 5];

    const s1$ = from(values);
  
    const results = [1, 5];
    let i = 0;

    s1$.pipe(
      filter((v, i) => v % 2 !== 0 && i !== 2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      }
    })

  });

  test('should not do nothing with error ', done => {
    const values = [1, 2, 3, 4, 5];
    const results = [1, 3, 5];
    let i = 0;
    const error = 'Some bug';
    const s1$ = from(values);
    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 500)

    s1$.pipe(
      mergeWith(s2$),
      filter(v => v % 2 !== 0)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      }
    })

  });
});