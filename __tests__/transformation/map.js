import { from, delay, map, Subject, mergeWith } from "rxjs"; 


describe('map', () => {

  test('should return modified values and works the same as JS map', done => {
    // s1$ --1--2--3--4--5--|-->  map(**2) 
    //                                     --1--4--9--16--25--|-->
    
    const values = [1, 2, 3, 4, 5];

    const s1$ = from(values).pipe(delay(1000));
  
    const results = values.map(v => v ** 2);
    let i = 0;

    s1$.pipe(
      map(v => v ** 2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      complete: done
    })

  });

  test('should not do nothing with error ', done => {
    
    
    const values = [1, 2, 3, 4, 5];

    const s1$ = from(values).pipe(delay(1000));
  
    const results = values.map(v => v ** 2);
    let i = 0;

    let error = 'Some bug';

    const s2$ = new Subject();

    setTimeout(() => {
      s2$.error(new Error(error));
    }, 1100)

    s1$.pipe(
      mergeWith(s2$),
      map(v => v ** 2)
      )            
    .subscribe({
      next: val => {
        expect(val).toEqual(results[i]);
        i++;
      },
      error: err => {
        expect(err.message).toEqual(error);
        done();
      },
      complete: done
    })

  });
});