import {from, scheduled, asapScheduler, asyncScheduler, } from 'rxjs';

describe('Scheduled', () => {
  // test('should run from sync', () => {
    
  //   const arr = [1, 2, 3];
  //   const s1$ = from(arr);
  //   let i = 0;
  //   s1$.subscribe({
  //     next: v => {
  //       console.log(v);
  //       expect(v).toEqual(arr[i]);
  //       i++;
  //     }
  //   })

  //   console.log('Sync log');

  // });

  test('should run from async', done => {
    
    const arr = [1];
    const s1$ = from(arr);
    // let i = 0;
    const s2$ = scheduled(s1$, asyncScheduler);
    s2$.subscribe({
      next: v => {
        console.log(v);
        expect(v).toEqual(1);
        // i++;
      },
      complete: done
    })

    console.log('Sync log');
    
  });
});