import {of} from 'rxjs'

describe('of()', () => {

  test('should return all arguments and complete the stream', () => {
    // --of(1, 2, 3)---------> --1--2--3--complete
    //  --of([1, 2, 3])---------> --[1, 2, 3]--complete
    const res = [1, 2, 3];
    
    const stream$ = of(...res);
    const s2$ = of(res)

    let i = 0;
    let completed = false;

    stream$.subscribe({
      next: v => {
        expect(v).toEqual(res[i]);
        i++;
      },
      complete: () => {
        completed = true;
      }
    })

    expect(completed).toEqual(true);

    s2$.subscribe(val => expect(val).toEqual(res));
  });
  
});