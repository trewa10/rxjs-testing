import {iif, from, EMPTY} from 'rxjs';

describe('iif', () => {
  
  test('should check boolean and return first or second observable ', () => {
    // stream1$ ----val----->
    // stream2$ ----val2----->
    // iif(true, stream1$, stream2$) --> stream1$
    // iif(false, stream1$, stream2$) --> stream2$

    let condition = false;

    const stream$ = iif(() => condition, from(['first']), from(['second']));

    stream$.subscribe(val => {
      expect(val).toEqual('second');
    })

    condition = true;

    stream$.subscribe(val => {
      expect(val).toEqual('first');
    })
  });

  test('should work with permissions checking', () => {

    let hasAccess;
    let completed = false;

    const permissionsCheck = () => hasAccess || false;

    const stream$ = iif(permissionsCheck, from(['success']), EMPTY);

    stream$.subscribe({
      // next: val => {
      //   expect(val).not.toEqual('success');
      // },
      complete: () => completed = true
    })

    expect(completed).toEqual(true);

    hasAccess = true;

    stream$.subscribe(val => {
      expect(val).toEqual('success');
    })
    
  });

});