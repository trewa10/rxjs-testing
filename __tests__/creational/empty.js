import {empty, EMPTY} from 'rxjs';

describe('EMPTY', () => {

  test('should return no value and complete', () => {

    // EMPTY --> -----complete--|

    let next = false;
    let completed = false;

    const emptyStr$ = EMPTY;

    emptyStr$.subscribe({
      next: () => next = true,
      complete: () => completed = true
    });

    expect(next).not.toEqual(true);
    expect(completed).toEqual(true);
    
  });
  
});