import {from} from 'rxjs'

describe('from()', () => {
  
  test('should return stream from array', () => {
    // from([1, 2, 3]) ---> --1--2--3
    
    const array = [1, 2, 3];
    let stream$ = from(array);

    let count = 0;
    let count2 = 0;
    
    stream$.subscribe(val => {
        let res = array[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.subscribe(val => {
        
        let res = array[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );

  });

  test('should return stream from string (or other iterable)', () => {
    // from([1, 2, 3]) ---> --1--2--3
    
    const str = 'hello';
    let stream$ = from(str);

    let count = 0;
    let count2 = 0;
    
    stream$.subscribe(val => {
        let res = str[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.subscribe(val => {
        
        let res = str[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );

  });

  test('should return stream from Promise', async () => {
    // from(Promise) ---> stream$ which returns value when promise is resolved
    
    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve('resolved')
      }, 500)
    });
    
    await promise; //only for jest tests
    let stream$ = from(promise);
    
    stream$.subscribe(val => {
        expect(val).toEqual('resolved');
      }
    );

  });


});