import { Subject } from 'rxjs'


describe('new Subject()', () => {
  
  test('should return next value to subscribers', () => {
    // --subscribe1--val---subscribe2---val2-------> subscribe1(val1, val2) subscribe2(val2)
    
    const stream$ = new Subject();
    
    const results = [1, 2, 3];

    let count = 0;
    let count2 = 1;
    
    stream$.subscribe(val => {
        let res = results[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.next(results[0]);

    stream$.subscribe(val => {
        
        let res = results[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );
    stream$.next(results[1]);
    stream$.next(results[2]);
  });

  test('should execute complete section after complete() method', () => {
    // --subscribe1--val1--val2--complete()-|--val3---> subscribe1(val1, val2), stream is completed

    const stream$ = new Subject();
    const results = [1, 2, 3];
    let count = 0;
    let completed = false;

    
    stream$.subscribe({
      next: val => {
        let res = results[count];
        count++;
        expect(val).toEqual(res);
        expect(val).not.toEqual(results[2]);
      },
      complete: () => {
        completed = true;
      }
    });
    stream$.next(results[0]);
    stream$.next(results[1]);
    
    stream$.complete();
    expect(completed).toEqual(true);

    stream$.next(results[2]);
  });

  test('should caught error', () => {
    // --subscribe1--val1--val2--error-|--complete()--val3---> subscribe1(val1, val2), error, stream is completed

    const stream$ = new Subject();
    const results = [1, 2, 3];
    let count = 0;
    let completed = false;

    let isError = false;

    
    stream$.subscribe({
      next: val => {
        let res = results[count];
        count++;
        expect(val).toEqual(res);
        expect(val).not.toEqual(results[2]);
      },
      error: error => {
        isError = true;
        expect(error.message).toEqual('Something went wrong');
        // console.log('Error: ', error.message);
      },
      complete: () => {
        completed = true;
      }
    });
    stream$.next(results[0]);
    stream$.next(results[1]);

    stream$.error(new Error('Something went wrong'));
    
    stream$.complete();
    expect(completed).not.toEqual(true);

    stream$.next(results[2]);
  });

});


