import { AsyncSubject } from 'rxjs'

describe('AsyncSubject()', () => {

  test('should return only the last value and only after complete()', () => {
    // --val1--subscribe1--val2--subscribe2--val3--complete()------> subscribe1(val3) subscribe2(val3)
    
    const stream$ = new AsyncSubject();
    const results = [1, 2, 3];
    
    stream$.next(results[0]);
    

    stream$.subscribe(val => {
        let res = results[results.length - 1];
        expect(val).toEqual(res);
      }
    );

    stream$.next(results[1]);

    stream$.subscribe(val => {
        let res = results[results.length - 1];
        expect(val).toEqual(res);
      }
    );

    stream$.next(results[2]);

    // stream$.error(new Error('async subject error'))
    
    stream$.complete();

  });

  test('should not return any values without complete()', () => {
    // --val1--subscribe1--val2--subscribe2--val3--complete()------> subscribe1(val3) subscribe2(val3)
    
    const stream$ = new AsyncSubject();
    let isSub = false;
    
    stream$.next(1);
    

    stream$.subscribe({
      next: val => {
        expect(val).toEqual(3);
        isSub = true;
      },
      error: (error) => {
        expect(error.message).toEqual('async subject error');
      }
    }
    );

    stream$.next(2);
    stream$.next(3);

    stream$.error(new Error('async subject error'))
    // stream$.complete();
    
    expect(isSub).not.toEqual(true);
  });
  
});