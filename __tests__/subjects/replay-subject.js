import { ReplaySubject } from 'rxjs'


describe('new ReplaySubject()', () => {
  
  test('should return all previous and all next values to subscribers', () => {
    // --val1--val2--val3--subscribe1--val4---subscribe2---val5-------> subscribe1(val1, val2, val3, val4, val5) subscribe2(val1, val2, val3, val4, val5)
    
    const stream$ = new ReplaySubject();
    
    const results = [1, 2, 3, 4, 5];

    let count = 0;
    let count2 = 0;
    
    stream$.next(results[0]);
    stream$.next(results[1]);
    stream$.next(results[2]);

    stream$.subscribe(val => {
        let res = results[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.next(results[3]);

    stream$.subscribe(val => {
        
        let res = results[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );
    stream$.next(results[4]);
  });

  test('should return bufferSize number of previous and all next values to subscribers', () => {
    // --val1--val2--val3--subscribe1--val4---subscribe2---val5-------> subscribe1(val2, val3, val4, val5) subscribe2(val3, val4, val5)
    
    const stream$ = new ReplaySubject(2);
    
    const results = [1, 2, 3, 4, 5];

    let count = 1;
    let count2 = 2;
    
    stream$.next(results[0]);
    stream$.next(results[1]);
    stream$.next(results[2]);

    stream$.subscribe(val => {
        let res = results[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.next(results[3]);

    stream$.subscribe(val => {
        
        let res = results[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );
    stream$.next(results[4]);
  });

});


