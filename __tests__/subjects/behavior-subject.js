import { BehaviorSubject } from 'rxjs'


describe('new BehaviorSubject()', () => {
  
  test('should return one previous and all next values to subscribers', () => {
    // --val1--subscribe1--val2---subscribe2---val3-------> subscribe1(val1, val2, val3) subscribe2(val2, val3)
    
    const stream$ = new BehaviorSubject(0);
    
    const results = [1, 2, 3];

    let count = 0;
    let count2 = 1;
    
    stream$.next(results[0]);

    stream$.subscribe(val => {
        let res = results[count];
        expect(val).toEqual(res);
        count++;
      }
    );

    stream$.next(results[1]);

    stream$.subscribe(val => {
        
        let res = results[count2];
        count2++;
        expect(val).toEqual(res);
      }
    );
    stream$.next(results[2]);
  });

  test('should return default value', () => {
    // --defaultVal--subscribe1--val1-------> subscribe1(defaultVal, val1)
    const stream$ = new BehaviorSubject(0);
    
    const results = [0, 1];

    let count = 0;
    
    stream$.subscribe(val => {
      let res = results[count];
      expect(val).toEqual(res);
      count++;
    });
    
    stream$.next(results[1]);
  });

});


